package com.realpacific.marvelapi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.realpacific.marvelapi.adapter.ComicsAdapter;
import com.realpacific.marvelapi.entity.Result;
import com.realpacific.marvelapi.utils.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CharacterDetailsActivity extends AppCompatActivity {
    Result result;
    @BindView(R.id.detail_thumbnail) ImageView imageView;
    @BindView(R.id.detail_description) TextView txtDescription;
    @BindView(R.id.detail_name) TextView txtName;
    @BindView(R.id.details_comics)
    RecyclerView rvComics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_details);

        ButterKnife.bind(this);

        if(getIntent() != null) {
            result = (Result) getIntent().getSerializableExtra("CHARACTER");
            initUI();
        }else {
            finish();
        }


        ComicsAdapter adapter = new ComicsAdapter(this, result.getComics().getItems());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvComics.setLayoutManager(layoutManager);
        rvComics.setAdapter(adapter);
        rvComics.setHasFixedSize(true);
        rvComics.setNestedScrollingEnabled(false);

    }

    private void initUI() {
        setTitle(result.getName());
        Glide.with(this).load(Utilities.convertToThumbnailUrl(result.getThumbnail().getPath(), result.getThumbnail().getExtension())).into(imageView);
        txtName.setText(result.getName());
        txtDescription.setText(result.getDescription());
    }
}

package com.realpacific.marvelapi.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.realpacific.marvelapi.CharacterDetailsActivity;
import com.realpacific.marvelapi.utils.Constants;
import com.realpacific.marvelapi.R;
import com.realpacific.marvelapi.entity.Result;
import com.realpacific.marvelapi.utils.Utilities;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.internal.Util;

public class MarvelAdapter extends RecyclerView.Adapter<MarvelAdapter.MarvelHolder> {
    private LayoutInflater inflater;
    private Context context;
    private List<Result> mList;

    public MarvelAdapter(Context context, List<Result> mList) {
        this.context = context;
        this.mList = mList;
        this.inflater = LayoutInflater.from(context);

    }

    @NonNull
    @Override
    public MarvelHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.single_marvel_layout, parent, false);
        return new MarvelHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MarvelHolder holder, int position) {
        Result result = mList.get(position);
        holder.insertIntoCurrentRow(result);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MarvelHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.character_name) TextView txtName;
        @BindView(R.id.character_thumbnails)
        ImageView imgThumbnail;
        @BindView(R.id.character_desc) TextView txtDescription;

        MarvelHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, CharacterDetailsActivity.class);
                    int position = getAdapterPosition();
                    intent.putExtra("CHARACTER", mList.get(position));
                    context.startActivity(intent);
                }
            });
        }

        void insertIntoCurrentRow(Result result) {
            Glide.with(context).load(Utilities.convertToThumbnailUrl(result.getThumbnail().getPath(), result.getThumbnail().getExtension())).into(imgThumbnail);
            txtName.setText(result.getName());
            txtDescription.setText(result.getDescription());
        }
    }
}

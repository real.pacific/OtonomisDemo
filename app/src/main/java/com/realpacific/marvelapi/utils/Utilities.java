package com.realpacific.marvelapi.utils;

/**
 * Created by prashant on 2018-04-04.
 */

public class Utilities {
    private Utilities(){}

    public static String convertToThumbnailUrl(String url, String extension){
        StringBuilder imagePath = new StringBuilder(url).append("/").append(Constants.IMAGE_TYPE).append(".").append(extension);
        return imagePath.toString();
    }

}

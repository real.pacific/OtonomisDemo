package com.realpacific.marvelapi.utils;


public class Constants {
    public static final String BASE_URL = "http://gateway.marvel.com/";
    public static final String IMAGE_TYPE = "portrait_xlarge";
}

package com.realpacific.marvelapi;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.realpacific.marvelapi.adapter.MarvelAdapter;
import com.realpacific.marvelapi.entity.Marvel;
import com.realpacific.marvelapi.entity.Result;
import com.realpacific.marvelapi.services.MarvelServices;
import com.realpacific.marvelapi.services.servicesimpl.MarvelServicesImpl;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MarvelServices.OnMarvelResponse{

    MarvelAdapter adapter;
    List<Result> mList;
    @BindView(R.id.progress_bar_container)
    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mList = new ArrayList<>();

        frameLayout.setVisibility(View.VISIBLE);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new MarvelServicesImpl().retrieveResponseFromServer(this);

        RecyclerView recyclerView = findViewById(R.id.main_rv);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new MarvelAdapter(this, mList);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(List<Result> results) {
        frameLayout.setVisibility(View.GONE);
        for(Result r : results){
            mList.add(r);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onFailure(String error) {
        Toast.makeText(this, "Error occurred.", Toast.LENGTH_SHORT).show();
    }
}

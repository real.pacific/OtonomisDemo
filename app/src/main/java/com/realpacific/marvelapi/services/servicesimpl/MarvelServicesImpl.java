package com.realpacific.marvelapi.services.servicesimpl;

import android.support.annotation.NonNull;
import android.util.Log;

import com.realpacific.marvelapi.utils.Constants;
import com.realpacific.marvelapi.services.MarvelClient;
import com.realpacific.marvelapi.entity.Marvel;
import com.realpacific.marvelapi.services.MarvelServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MarvelServicesImpl implements MarvelServices {
    @Override
    public void retrieveResponseFromServer(final OnMarvelResponse callback) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();

        MarvelClient client = retrofit.create(MarvelClient.class);
        Call<Marvel> call = client.getServerRequest();
        call.enqueue(new Callback<Marvel>() {
            @Override
            public void onResponse(@NonNull Call<Marvel> call, @NonNull Response<Marvel> response) {
                Log.i("~~~", "onResponse: " +response.toString());
                if(response.isSuccessful())
                    callback.onSuccess(response.body().getData().getResults());
                else
                    callback.onFailure(response.errorBody().toString());
            }

            @Override
            public void onFailure(@NonNull Call<Marvel> call, @NonNull Throwable t) {
                Log.i("~~~", "onFailure: " + t);
                callback.onFailure(t.toString());
            }
        });
    }
}

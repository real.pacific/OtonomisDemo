package com.realpacific.marvelapi.services;

import com.realpacific.marvelapi.entity.Marvel;
import com.realpacific.marvelapi.entity.Result;

import java.util.List;


public interface MarvelServices {
    void retrieveResponseFromServer(OnMarvelResponse callback);

    interface OnMarvelResponse{
        void onSuccess(List<Result> result);
        void onFailure(String error);
    }
}

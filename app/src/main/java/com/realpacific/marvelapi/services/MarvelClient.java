package com.realpacific.marvelapi.services;

import com.realpacific.marvelapi.entity.Marvel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MarvelClient {

    @GET("/v1/public/characters?ts=1&apikey=f140603047ca67d40f2ac91a30e62f14&hash=e94886fd48ff7c919b4c4e0f9185baf5")
    Call<Marvel> getServerRequest();

    @GET("/v1/public/characters/{characterId}?ts=1&apikey=f140603047ca67d40f2ac91a30e62f14&hash=e94886fd48ff7c919b4c4e0f9185baf5")
    Call<Marvel> getServerRequestForIndividualCharacter(@Path("characterId") String id);

}
